#!/usr/bin/env bash
# Copyright (c) 2021 Collabora, Ltd.
#
# SPDX-License-Identifier: BSL-1.0

set -e

# shellcheck disable=SC2086
ROOT=$(cd "$(dirname $0)" && pwd)

DEFAULT_NDK_VERSION=21.3.6528147
PROJECT=percetto
RELATIVE_SOURCE_DIR=percetto-lib
ANDROID_STL=c++_shared

ANDROID_NDK_HOME=${ANDROID_NDK_HOME:-${HOME}/Android/Sdk/ndk/${DEFAULT_NDK_VERSION}}

if [ ! -f "${ANDROID_NDK_HOME}/build/cmake/android.toolchain.cmake" ]; then
    echo "Please set ANDROID_NDK_HOME to a valid, installed NDK!"
    exit 1
fi

BUILD_DIR=${BUILD_DIR:-${ROOT}/build-android}
INSTALL_DIR=${INSTALL_DIR:-${ROOT}/build-android-install}
SOURCE_DIR=${ROOT}/${RELATIVE_SOURCE_DIR}
GENERATION_DIR="${BUILD_DIR}/x86"

rm -f "${GENERATION_DIR}/*.pom" || true

rm -rf "${INSTALL_DIR}"
for arch in x86 x86_64 armeabi-v7a arm64-v8a; do
    cmake -S "${SOURCE_DIR}" \
        -B "${BUILD_DIR}/${arch}" \
        -G Ninja \
        "-DCMAKE_TOOLCHAIN_FILE=${ANDROID_NDK_HOME}/build/cmake/android.toolchain.cmake" \
        "-DCMAKE_ANDROID_NDK=${ANDROID_NDK_HOME}" \
        -DANDROID_ABI=${arch} \
        -DANDROID_PLATFORM=24 \
        -DANDROID_STL=${ANDROID_STL} \
        "-DCMAKE_INSTALL_PREFIX=${INSTALL_DIR}/${PROJECT}" \
        -DCMAKE_BUILD_TYPE=RelWithDebInfo \
        -DINSTALL_FOR_PREFAB=ON
    ninja -C "${BUILD_DIR}/${arch}"
    ninja -C "${BUILD_DIR}/${arch}" install
    # for comp in License Headers Loader Prefab; do
    #     cmake -DCMAKE_INSTALL_COMPONENT=${comp} -P ${BUILD_DIR}/${arch}/cmake_install.cmake
    # done
done
# find latest pom
DECORATED=$(cd "${GENERATION_DIR}" && find . -maxdepth 1 -name "${PROJECT}*.pom" | sort | tail -n 1)
# strip leading ./
DECORATED=${DECORATED#./}
DECORATED_STEM=${DECORATED%.pom}
VERSION=${DECORATED_STEM#${PROJECT}-}
DIR=$(pwd)/maven_repo/org/freedesktop/monado/deps/${PROJECT}/${VERSION}

mkdir -p "${DIR}"
cp "${GENERATION_DIR}/${DECORATED}" "${ROOT}"
cp "${GENERATION_DIR}/${DECORATED}" "${DIR}"

(
    cd "$INSTALL_DIR/${PROJECT}"
    7za a -r ../${PROJECT}.zip ./*
    mv ../${PROJECT}.zip "$ROOT/${DECORATED_STEM}.aar"
    cp "$ROOT/${DECORATED_STEM}.aar" "$DIR/${DECORATED_STEM}.aar"
)
