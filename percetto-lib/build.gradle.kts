// Copyright 2020-2021, Collabora, Ltd.
// SPDX-License-Identifier: BSL-1.0

plugins {
    id("com.android.library")
    `maven-publish`
}

apply(from = file("../shared.gradle"))

// Must copy headers into their own directory for Prefab.
val headerDir = File(project.buildDir, "generated/headers")

val stageHeaders = task("stageHeaders", Copy::class) {
    from("percetto/src/") {
        include("*.h")
    }
    into(File(headerDir, "percetto"))
}

tasks["preBuild"].dependsOn(stageHeaders)


android {

    compileSdkVersion(30)
    buildToolsVersion = "30.0.2"
    defaultConfig {
        minSdkVersion(24)
        targetSdkVersion(30)
        versionCode = 1
        versionName = project.version.toString()

        externalNativeBuild {
            cmake {
                arguments(
                    "-DANDROID_STL=c++_shared",
                    "-DPERFETTO_SDK_PATH=${project.rootDir.path}/perfetto-lib/perfetto/sdk"
                )
            }
        }
    }
    externalNativeBuild {
        cmake {
            path("CMakeLists.txt")
        }
    }

    buildFeatures {
        buildConfig = false
        prefabPublishing = true
    }
    prefab {
        // create("perfetto")
        create("percetto") {
            this.headers = headerDir.toString()
        }
    }
    ndkVersion = "21.3.6528147"
}

publishing {
    publications {
        create<MavenPublication>("maven") {
            artifactId = "percetto"
            afterEvaluate {
                artifact(tasks.getByName("bundleReleaseAar"))
            }
            pom {
                licenses {
                    license {
                        name.set("Apache-2.0")
                        url.set("http://www.apache.org/licenses/LICENSE-2.0.txt")
                    }
                }
            }
        }
    }
    repositories {
        maven {
            url = uri(layout.projectDirectory.dir("repo"))
        }
    }
}
