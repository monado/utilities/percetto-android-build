// Copyright 2021, Collabora, Ltd.
// SPDX-License-Identifier: BSL-1.0

buildscript {
    repositories {
        mavenCentral()
    }
}

plugins {
    id("maven-publish")
}

apply(from = file("../shared.gradle"))

publishing {
    publications {
        create<MavenPublication>("maven") {
            artifactId = "percetto"
            afterEvaluate {
                artifact(File(projectDir, "../percetto-${version}.aar"))
            }
            pom {
                name.set("PerCetto and Perfetto")
                description.set("System profiling, app tracing, and trace analysis, plus the PerCetto C wrapper library. Build by/for the Monado project.")
                url.set("https://gitlab.freedesktop.org/monado/utilities/percetto-android-build")
                licenses {
                    license {
                        name.set("Apache-2.0")
                        url.set("http://www.apache.org/licenses/LICENSE-2.0.txt")
                    }
                }
                scm {
                    connection.set("scm:git:https://gitlab.freedesktop.org/monado/utilities/percetto-android-build.git")
                    developerConnection.set("scm:git:git@gitlab.freedesktop.org:monado/utilities/percetto-android-build.git")
                    url.set("https://gitlab.freedesktop.org/monado/utilities/percetto-android-build")
                }
            }
        }
    }

    repositories {
        val jobToken = System.getenv("CI_JOB_TOKEN")
        val privateToken = project.findProperty("fdoGitlab.privateToken") as String?
        val deployToken = project.findProperty("fdoGitlab.deployToken") as String?
        if (jobToken != null && jobToken.isNotEmpty()) {
            println("Have CI token, can publish to Gitlab")
            maven {
                name = "gitlab"
                url = uri("https://gitlab.freedesktop.org/api/v4/projects/9792/packages/maven")
                credentials(HttpHeaderCredentials::class) {
                    name = "Job-Token"
                    value = jobToken
                }
                authentication {
                    create("header", HttpHeaderAuthentication::class.java)
                }
            }
        } else if (deployToken != null) {
            println("Have deploy token, can publish to Gitlab")
            maven {
                name = "gitlab"
                url = uri("https://gitlab.freedesktop.org/api/v4/projects/9792/packages/maven")
                credentials(HttpHeaderCredentials::class) {
                    name = "Deploy-Token"
                    value = deployToken
                }
                authentication {
                    create("header", HttpHeaderAuthentication::class.java)
                }
            }
        } else if (privateToken != null) {
            println("Have private token, can publish to Gitlab")
            maven {
                name = "gitlab"
                url = uri("https://gitlab.freedesktop.org/api/v4/projects/9792/packages/maven")
                credentials(HttpHeaderCredentials::class) {
                    name = "Private-Token"
                    value = privateToken
                }
                authentication {
                    create("header", HttpHeaderAuthentication::class.java)
                }
            }
        }
        // This option is always available.
        maven {
            url = uri(layout.buildDirectory.dir("repo"))
        }

    }
}
